require('dotenv').config();

const compression = require('compression');
const express = require('express');
const app = express();
const stylus = require('express-stylus');
const join = require('path').join;
const publicDir = join(__dirname, '/public');

const Discord = require('discord.io');
const discordBot = new Discord.Client({
	token: process.env.BOT_TOKEN,
	autorun: true
});

const TeleBot = require('telebot');
const telegramBot = new TeleBot(process.env.TELEGRAM_TOKEN);

const Gfycat = require('gfycat-sdk');
const gfycat = new Gfycat({
	clientId: process.env.GFYCAT_ID,
	clientSecret: process.env.GFYCAT_SECRET
});

const rp = require('request-promise');
const os = require('os');

// Discord
discordBot.on('message', function(user, userID, channelID, message, rawEvent) {
	if (message.substring(0, 1) == '>') {
		var command = message.substring(1);

		if (command === 'help') {
			discordBot.sendMessage({
				to: channelID,
				message: 'Have a look there http://redditlist.com/all'
			});
		} else if (command === 'ping') {
			discordBot.sendMessage({
				to: channelID,
				message: 'pong'
			});
		} else {
			rp({
				uri: 'http://www.reddit.com/r/' + command + '.json',
				json: true
			})
				.then(function(response) {
					discordBot.sendMessage({
						to: channelID,
						message:
							response.data.children[
								Math.floor(Math.random() * response.data.children.length)
							].data.url
					});
				})
				.catch(function(err) {
					discordBot.sendMessage({
						to: channelID,
						message:
							'Subreddit not found, have a look there http://redditlist.com/all'
					});
				});
		}
	}
});

discordBot.on('ready', function() {
	console.log('Discord Bot launched.');
});

// Telegram
telegramBot.on('text', msg => {
	if (msg.text.substring(0, 1) === '/') {
		var command = msg.text.substring(1).toLowerCase();
		if (command.indexOf('@') > -1) {
			var command = command.substr(0, command.indexOf('@'));
		}
	} else {
		return;
	}

	if (command === 'start') {
		return telegramBot.sendMessage(
			msg.chat.id,
			"Benvenuto! Se non conosci nessuna subreddit dai un'occhiata qui http://redditlist.com/all" +
				os.EOL +
				os.EOL +
				'Oppure scrivi /zap'
		);
	} else if (command === 'aiuto') {
		return telegramBot.sendMessage(
			msg.chat.id,
			'Seleziona un comando:' +
				os.EOL +
				os.EOL +
				'/zap' +
				os.EOL +
				'/lenny' +
				os.EOL +
				'/randnsfw (attenzione ai cazzi)'
		);
	} else if (command.indexOf('cock') > -1) {
		return telegramBot.sendDocument(
			msg.chat.id,
			'https://media.giphy.com/media/KCmA9He637oic/giphy.gif',
			{
				caption: 'AH! Gayyyyyyyyyyyyyyyyyyyyyyyy',
				notification: false
			}
		);
	} else if (command === 'zap') {
		let replyMarkup = telegramBot.keyboard(
			[
				['/tits', '/ass', '/pussy'],
				['/boltedontits', '/rearpussy'],
				['/bustypetite', '/collegesluts'],
				['/gonewild', '/petitegonewild'],
				['/porninfifteenseconds', '/nsfw_gif'],
				['/darkangels', '/asianhotties'],
				['/realgirls', '/amateur'],
				['/adorableporn', '/happyembarrassedgirls'],
				['/tittydrop', '/biggerthanyouthought']
			],
			{
				resize: true
			}
		);
		return telegramBot.sendMessage(
			msg.chat.id,
			'Impugna saldamente la lancia' + os.EOL + '👊✊💦 ( . Y . )',
			{
				replyMarkup
			}
		);
	} else if (command === 'lenny') {
		let replyMarkup = telegramBot.keyboard(
			[
				['( ͡° ͜ʖ ͡°)', 'ᕕ( ͡° ͜ʖ ͡° )ᕗ', '( ͠ ͠° ل͜  ͠°)'],
				['ಠ_ಠ', 'ಠ‿ಠ', 'ಠ‿↼'],
				['ಥ‿ಥ', '( ͡°╭͜ʖ╮͡° )', '(☞ﾟヮﾟ)☞'],
				['(ノಠ益ಠ)ノ', '୧༼ಠ益ಠ༽୨', '(/ﾟДﾟ)/'],
				['｡゜(｀Д´)゜｡', '¯\\_(ツ)_/¯', 'o(-`д´- ｡)'],
				['(╯°□°）╯︵ ┻━┻', '┬─┬ノ( º _ ºノ)'],
				['(ノಠ益ಠ)ノ彡┻━┻', '(ノ꒪Д꒪)ノ'],
				['(⚆_⚆)', '(¬▂¬)', '(´･_･`)'],
				['ᕕ(ᐛ)ᕗ', '¯\\(◉‿◉)/¯', '(͡๏̯͡๏)']
			],
			{
				resize: true
			}
		);

		return telegramBot.sendMessage(msg.chat.id, '( ͡° ͜ʖ ͡°)', {
			replyMarkup
		});
	} else if (command === 'patchlog') {
		return telegramBot.sendMessage(
			msg.chat.id,
			'Versione 4.🔎🍑' +
				os.EOL +
				"È arrivato l'ordinamento! Non ho inserito misure di sicurezza, non so nemmeno se crasha a comando errato e sicuro crasherà nel gruppo, quindi prestate attenzione:" +
				os.EOL +
				os.EOL +
				'@RedditChicksBot [nome subreddit] [top | hot | new | controversal] [se al precedente avete messo top potete aggiungere: [day | week | month | year | all]' +
				os.EOL +
				os.EOL +
				'Esempio per Da Fiume: scrivi @RedditChicksBot rearpussy top day'
		);
	} else if (command !== 'start') {
		rp({
			uri: 'http://www.reddit.com/r/' + command + '.json',
			json: true
		})
			.then(function(response) {
				var pairedRandom = Math.floor(
					Math.random() * response.data.children.length
				);
				let url = response.data.children[pairedRandom].data.url;
				let title = response.data.children[pairedRandom].data.title;
				if (url.indexOf('imgur.com/a/') > -1 || url.indexOf('/gallery/') > -1) {
					var imgurAlbum = url.substr(url.lastIndexOf('/') + 1);
					rp({
						uri: 'https://api.imgur.com/3/album/' + imgurAlbum,
						headers: { Authorization: 'Client-ID ' + process.env.IMGUR_ID },
						json: true
					})
						.then(function(response) {
							return response.data.images.map(function(element) {
								return telegramBot.sendPhoto(msg.chat.id, element.link, {
									caption: element.description || element.title || element.id,
									notification: false
								});
							}, this);
						})
						.catch(function(err) {
							console.log(err);
							telegramBot.sendMessage(
								msg.chat.id,
								'Qualcosa è andato storto 🤔',
								{ notification: false }
							);
						});
				} else if (url.indexOf('gfycat.com') > -1) {
					var gfycatFile = url.substr(url.lastIndexOf('/') + 1);
					let options = {
						gfyId: gfycatFile
					};
					gfycat
						.getGifDetails(options)
						.then(data =>
							telegramBot.sendVideo(msg.chat.id, data.gfyItem.mobileUrl, {
								caption: title,
								notification: false
							})
						)
						.catch(function(err) {
							console.log(err);
							telegramBot.sendMessage(
								msg.chat.id,
								'Qualcosa è andato storto 🤔',
								{ notification: false }
							);
						});
				} else if (url.indexOf('.gifv') > -1) {
					var video = url.slice(0, -5) + '.mp4';
					return telegramBot.sendVideo(msg.chat.id, video, {
						caption: response.data.children[pairedRandom].data.title,
						notification: false
					});
				} else if (url.indexOf('.gif') > -1) {
					return telegramBot.sendDocument(msg.chat.id, url, {
						caption: response.data.children[pairedRandom].data.title,
						notification: false
					});
				} else if (url.indexOf('.jpg') > -1 || url.indexOf('.png') > -1) {
					return telegramBot.sendPhoto(msg.chat.id, url, {
						caption: response.data.children[pairedRandom].data.title,
						notification: false
					});
				} else {
					return telegramBot.sendMessage(
						msg.chat.id,
						response.data.children[pairedRandom].data.title +
							os.EOL +
							response.data.children[pairedRandom].data.url,
						{ notification: false }
					);
				}
			})
			.catch(function(err) {
				console.log(err);
				return telegramBot.sendMessage(
					msg.chat.id,
					"Subreddit non trovata! Dai un'occhiata qui http://redditlist.com/all",
					{ webPreview: false, notification: false }
				);
			});
	} else {
		return telegramBot.sendMessage(msg.chat.id, 'Qualcosa è andato storto 🤔');
	}
});

telegramBot.on('inlineQuery', msg => {
	function getPosition(string, subString, index) {
		return string.split(subString, index).join(subString).length;
	}

	if (msg.query.indexOf(' ') > -1) {
		let query = msg.query.substring(0, getPosition(msg.query, ' ', 1));

		let sort = msg.query.substring(
			getPosition(msg.query, ' ', 1) + 1,
			getPosition(msg.query, ' ', 2)
		);

		let time = msg.query.substring(
			getPosition(msg.query, ' ', 2) + 1,
			msg.query.length
		);

		const answers = telegramBot.answerList(msg.id, { cacheTime: 0 });

		rp({
			uri:
				'http://www.reddit.com/r/' +
				query +
				'/' +
				sort +
				'.json?sort=' +
				sort +
				'&t=' +
				time,
			json: true
		})
			.then(function(response) {
				response.data.children.forEach(function(element) {
					answers.addPhoto({
						id: element.data.id,
						caption: element.data.title,
						photo_url: element.data.url,
						thumb_url: element.data.url
					});
				}, this);
				return telegramBot.answerQuery(answers);
			})
			.catch(function(err) {
				console.log(err);
			});
	} else {
		let query = msg.query.substring(0, getPosition(msg.query, ' ', 1));

		let sort = msg.query.substring(
			getPosition(msg.query, ' ', 1) + 1,
			getPosition(msg.query, ' ', 2)
		);

		let time = msg.query.substring(
			getPosition(msg.query, ' ', 2) + 1,
			msg.query.length
		);

		const answers = telegramBot.answerList(msg.id, { cacheTime: 0 });

		rp({
			uri:
				'http://www.reddit.com/r/' +
				query +
				'/' +
				sort +
				'.json?sort=' +
				sort +
				'&t=' +
				time,
			json: true
		})
			.then(function(response) {
				response.data.children.forEach(function(element) {
					answers.addPhoto({
						id: element.data.id,
						caption: element.data.title,
						photo_url: element.data.url,
						thumb_url: element.data.url
					});
				}, this);
				return telegramBot.answerQuery(answers);
			})
			.catch(function(err) {
				console.log(err);
			});
	}
});

telegramBot.start();

// Server
app.use(compression());
app.set('view engine', 'pug');

app.use(
	stylus({
		src: publicDir
	})
);

app.set('port', process.env.PORT || 5000);
app.use(express.static(publicDir));

app.get('/', function(req, res) {
	res.render('index');
});

if (app.get('env') === 'development') {
	app.locals.pretty = true;
}

app.listen(app.get('port'), function() {
	console.log('Node app is running at localhost:' + app.get('port'));
});

// const cluster = require('cluster');
// const numCPUs = require('os').cpus().length;

// if (cluster.isMaster) {
// 	console.log(`Master ${process.pid} is running`);

// 	// Fork workers.
// 	for (let i = 0; i < process.env.WEB_CONCURRENCY || 0; i++) {
// 		cluster.fork();
// 	}

// 	cluster.on('exit', (worker, code, signal) => {
// 		console.log(`worker ${worker.process.pid} died`);
// 	});
// } else {
// 	app.listen(app.get('port'), function() {
// 		console.log('Node app is running at localhost:' + app.get('port'));
// 	});

// 	console.log(`Worker ${process.pid} started`);
// }
